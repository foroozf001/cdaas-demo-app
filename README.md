# Spring Boot Demo App
This solution demonstrates the building and then deploying of a containerized Spring Boot application to an AWS EC2 instance by means of an automated CI/CD pipeline. The automated pipeline launches after a repository check-in. The pipeline stages are described in ```.gitlab-ci.yml```. The building and deploying of artifacts happen in seperate stages. 

## Continous integration (CI) part
The instructions in the build-stage are docker-commands that log into gitlab container registry, build the artifact (a docker image), and then push the artifact to [gitlab container registry](https://gitlab.com/foroozf001/cdaas-demo-app/container_registry). This build-stage is described in technical detail in [gitlab official documentation](https://docs.gitlab.com/ee/user/packages/container_registry/#configure-your-gitlab-ciyml-file). This solution makes use of [predefined gitlab environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to get data from the gitlab registry, repository and pipeline.
```sh
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$DOCKER_IMAGE:$CI_PIPELINE_ID .
docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$DOCKER_IMAGE:$CI_PIPELINE_ID
```
## Continuous deploying (CD) part
The instructions in the deployment-stage configure the agent and then run an ansible playbook. Because there is no official ansible image to run on the agent, we resort to an image maintained by a community user. The ```dockerfile``` of this image is found in the [gitlab repository](https://gitlab.com/torese/docker-ansible/-/blob/master/Dockerfile). Essentially the image is a centos image with an ansible installation.
```yml
image: registry.gitlab.com/torese/docker-ansible
```
The image lacks however an ssh-client which ansible needs to run the playbook on the target machine. Therefore the ssh-client is installed first.
```
yum install openssh-clients -y
```
The target machine allows for ssh connection. To establish an ssh-connection the agent needs to have a private key file present. This private key file is fetched from a gitlab environment variable (```$SSH_PRIVATE_KEY```), formatted, then written to ```~/.ssh/id_rsa```. 
```
ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa 2>/dev/null <<< y >/dev/null
rm -f ~/.ssh/id_rsa.pub
echo "-----BEGIN RSA PRIVATE KEY-----" > ~/.ssh/id_rsa
echo $SSH_PRIVATE_KEY | tr ' ' '\n' | tail -n+5 | head -n-4 >> ~/.ssh/id_rsa
echo "-----END RSA PRIVATE KEY-----" >> ~/.ssh/id_rsa
```
Before the ansible playbook is executed, the connection to the target machine is tested by performing a ping. The input for ansible ping module is an [inventory-file](https://gitlab.com/foroozf001/cdaas-demo-app/-/blob/master/hosts) which describes the target machines, the user to log into the target machine and the private key created earlier.
```sh
ansible target -i $HOSTS_FILE -m ping -u $VM_USER --private-key=~/.ssh/id_rsa
```
Once the ping succeeds, the ansible playbook is executed. Some environment variables are fed through to ansible that the playbook requires to log into the container registry, pull the image and run the container.
```sh
ansible-playbook playbook.yml -i $HOSTS_FILE -u $VM_USER --private-key=~/.ssh/id_rsa --extra-vars=gcr_user=$CI_REGISTRY_USER --extra-vars=gcr_access_token=$GCR_ACCESS_TOKEN --extra-vars=registry_url=$CI_REGISTRY --extra-vars=container_name=$DOCKER_IMAGE --extra-vars=container_image=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$DOCKER_IMAGE:$CI_PIPELINE_ID
```
The ansible playbook first configures the target machine as a docker host by installing docker engine. The instructions to set the target machine up as a docker host are described in technical detail in [official docker documentation](https://docs.docker.com/engine/install/ubuntu/).
```yml
tasks:
    - name: Install aptitude using apt
      apt: name=aptitude state=latest update_cache=yes force_apt_get=yes

    - name: Install required system packages
      apt: name={{ item }} state=latest update_cache=yes
      loop: [ 'apt-transport-https', 'ca-certificates', 'curl', 'gnupg', 'lsb-release', 'software-properties-common', 'python3-pip', 'virtualenv', 'python3-setuptools' ]

    - name: Add Docker GPG apt Key
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg

    - name: Add Docker Repository
      apt_repository:
        repo: deb https://download.docker.com/linux/ubuntu {{ vm_distro }} stable
        state: present

    - name: Update apt and install docker-ce
      apt: update_cache=yes name={{ item }} state=latest
      loop: [ 'docker-ce', 'docker-ce-cli', 'containerd.io' ]
```
Ansible installs a docker module that allows ansible to run common docker tasks.
```yml
    - name: Install Docker Module for Python
      pip:
        name: docker
```
To pull the image the build-stage built and pushed to gitlab container registry, the target machine needs to log into gitlab container registry.
```yml
    - name: Log into private registry and force re-authorization
      docker_login:
        registry_url: "{{ registry_url }}"
        username: "{{ gcr_user }}"
        password: "{{ gcr_access_token }}"
        reauthorize: yes
```
The image is then pulled from gitlab container registry.
```yml
    - name: Pull default docker image
      docker_image:
        name: "{{ container_image }}"
        source: pull
```
Ansible creates and runs the docker container on the host network instead of the bridge network. This allows for public access to the application.
```yml
    - name: Create default containers
      docker_container:
        name: "{{ container_name }}"
        image: "{{ container_image }}"
        state: started
        purge_networks: yes
        networks_cli_compatible: yes
        network_mode: host
```
After the playbook is finished, the Spring Boot demo application is successfully running on the target machine and is accessible on the following URLs:
[http://ec2-35-171-104-188.compute-1.amazonaws.com:8080/](http://ec2-35-171-104-188.compute-1.amazonaws.com:8080/) or [http://35.171.104.188:8080/](http://35.171.104.188:8080/).